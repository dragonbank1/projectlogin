<?php
$dbhost = '127.0.0.1';
$dbuser = '';
$dbpass = '';
$dbname = '';
try{
	$conmysql = new PDO("mysql:dbname={$dbname};host={$dbhost}", $dbuser, $dbpass);
	$conmysql->exec("set names utf8mb4");
}catch(PDOException $e){
	http_response_code(203);
	exit();
}
?>