<?php
ini_set('display_errors', false);
ini_set('error_log', __DIR__.'/error.log');
require('./connection.php');

$getUn = $conmysql->prepare("SELECT password,path_avatar,firstname,lastname FROM user WHERE username = :username");
$getUn->execute([':username' => $_POST["un"]]);
if($getUn->rowCount() > 0){
	$rowUn = $getUn->fetch(PDO::FETCH_ASSOC);
	if(password_verify($_POST["old_pw"],$rowUn["password"])){
		$getFiveLast = $conmysql->prepare("SELECT password_new FROM logchangepassword WHERE username = :un ORDER BY id_log DESC LIMIT 5");
		$getFiveLast->execute([':un' => $_POST["un"]]);
		while($rowFive = $getFiveLast->fetch(PDO::FETCH_ASSOC)){
			if(password_verify($_POST["new_pw"],$rowFive["password_new"])){
				echo 'ห้ามตั้งรหัสผ่านเหมือนกับ 5 ครั้งล่าสุด';
				exit();
			}
		}
		$updateNewPass = $conmysql->prepare("UPDATE user SET password = :new_pw WHERE username = :username");
		$updateNewPass->execute([
			':new_pw' => password_hash($_POST["new_pw"],PASSWORD_DEFAULT),
			':username' => $_POST["un"]
		]);
		$insertLastChg = $conmysql->prepare("INSERT INTO logchangepassword(password_old,password_new,username)
											VALUES(:old_pw,:new_pw,:un)");
		$insertLastChg->execute([
			':old_pw' => password_hash($_POST["old_pw"],PASSWORD_DEFAULT),
			':new_pw' => password_hash($_POST["new_pw"],PASSWORD_DEFAULT),
			':un' => $_POST["un"]
		]);
		echo 'เปลี่ยนรหัสผ่านเสร็จสิ้น';
	}else{
		echo 'รหัสผ่านเก่าไม่ถูกต้อง';
	}
}else{
	echo 'ไม่พบชื่อผุ้ใช้';
}
?>