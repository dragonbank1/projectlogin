<?php
ini_set('display_errors', false);
ini_set('error_log', __DIR__.'/error.log');
require('./connection.php');

$getUn = $conmysql->prepare("SELECT password,path_avatar,firstname,lastname FROM user WHERE username = :username");
$getUn->execute([':username' => $_POST["un"]]);
if($getUn->rowCount() > 0){
	$rowUn = $getUn->fetch(PDO::FETCH_ASSOC);
	if(password_verify($_POST["pw"],$rowUn["password"])){
		$arr = array();
		$arr["RESULT"] = TRUE;
		$arr["AVATAR"] = $rowUn["path_avatar"];
		$arr["NAME"] = $rowUn["firstname"].' '.$rowUn["lastname"];
		echo json_encode($arr);
	}else{
		$arr = array();
		$arr["RESULT"] = FALSE;
		$arr["RESPONSE"] = 'รหัสผ่านไม่ถูกต้อง';
		echo json_encode($arr);
	}
}else{
	$arr = array();
	$arr["RESULT"] = FALSE;
	$arr["RESPONSE"] = 'ไม่พบผู้ใช้งาน';
	echo json_encode($arr);
}
?>