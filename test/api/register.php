<?php
ini_set('display_errors', false);
ini_set('error_log', __DIR__.'/error.log');
require('./connection.php');

$getUn = $conmysql->prepare("SELECT username FROM user WHERE username = :username");
$getUn->execute([':username' => $_POST["un"]]);
if($getUn->rowCount() > 0){
	echo 'ชื่อผู้ใช้ซ้ำกับในระบบ';
}else{
	$response = null;
	if(isset($_FILES['file']['name'])){
		
	   $filename = $_FILES['file']['name'];

	   $location = "upload/".$filename;
	   $imageFileType = pathinfo($location,PATHINFO_EXTENSION);
	   $imageFileType = strtolower($imageFileType);
	   $valid_extensions = array("jpg","jpeg","png");

	   if(in_array(strtolower($imageFileType), $valid_extensions)) {
		  if(move_uploaded_file($_FILES['file']['tmp_name'],$location)){
			 $response = $location;
		  }
	   }

	}
	$insertUn = $conmysql->prepare("INSERT INTO user(username,password,firstname,lastname,path_avatar) 
									VALUES(:username,:password,:firstname,:lastname,:path_avatar)");
	if($insertUn->execute([
		':username' => $_POST["un"],
		':password' => password_hash($_POST["pw"],PASSWORD_DEFAULT),
		':firstname' => $_POST["fname"],
		':lastname' => $_POST["lname"],
		':path_avatar' => $response
	])){
		echo 'สมัครสมาชิกสำเร็จ';
	}else{
		echo 'สมัครสมาชิกล้มเหลว';
	}
}
?>